package com.yuhj.cateyemovie.adapter;

import com.yuhj.cateyemovie.R;
import com.yuhj.cateyemovie.bean.Comments;
import com.yuhj.cateyemovie.utils.ImageCache;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.R.layout;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @name ShortCommentAdapter
 * 短評的适配器
 * @Descripation <br>
 * @author 禹慧军
 * @date 2014-11-4
 * @version 1.0
 */
public class HotCommentAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<Comments> list;
	private LruCache<String,Bitmap> lruCache;
	public HotCommentAdapter(Context context) {
		this.context=context;
		lruCache= ImageCache.GetLruCache(context);
	}
	public void bindData(ArrayList<Comments> list){
		this.list = list;
	}

	@Override
	public int getCount() {
		return list.size();
	}
	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHoder viewHoder =null;
		if (convertView==null) {
			viewHoder = new ViewHoder();
			convertView = LayoutInflater.from(context).inflate(R.layout.hot_comment_item, null);
			viewHoder.content=(TextView) convertView.findViewById(R.id.txtComment);
			viewHoder.applyCount = (TextView) convertView.findViewById(R.id.commentCount);
			viewHoder.time=(TextView) convertView.findViewById(R.id.txtTime);
			viewHoder.userImage=(ImageView) convertView.findViewById(R.id.user_profile);
			viewHoder.username=(TextView) convertView.findViewById(R.id.txt_username);
			viewHoder.title=(TextView) convertView.findViewById(R.id.txtCommentTitle);
			convertView.setTag(viewHoder);
		}else {
			viewHoder=(ViewHoder) convertView.getTag();
		}
		Comments comments =list.get(position);
		viewHoder.applyCount.setText(comments.getReply()+"");
		viewHoder.content.setText(comments.getComment());
		viewHoder.time.setText(transferLongToDate("yyyy-MM-dd",
				Long.parseLong(comments.getTime())));
		viewHoder.username.setText(comments.getUser().getName());
		viewHoder.userImage.setTag(comments.getUser().getImage());
		viewHoder.userImage.setImageResource(R.drawable.bg_default_cat_gray);
		viewHoder.title.setText(comments.getTitle());
		new ImageCache(context, lruCache, viewHoder.userImage,comments.getUser().getImage(), "CatEyeMovie", 50, 50);
		return convertView;
	}
	
	private class ViewHoder{
		TextView username;
		TextView time;
		ImageView userImage;
		TextView title;
		TextView content;
		TextView diggCount;
		TextView applyCount;
		
	}
	
	private String transferLongToDate(String dateFormat, Long millSec) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		Date date = new Date(millSec);
		return sdf.format(date);
	}

}
