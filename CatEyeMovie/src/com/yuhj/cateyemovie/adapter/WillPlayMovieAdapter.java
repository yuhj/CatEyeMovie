package com.yuhj.cateyemovie.adapter;

import java.util.ArrayList;

import com.yuhj.cateyemovie.R;
import com.yuhj.cateyemovie.bean.PlayingMovieList;
import com.yuhj.cateyemovie.fragment.PalyingFragment;
import com.yuhj.cateyemovie.utils.ImageCache;
import com.yuhj.cateyemovie.widget.PinnedSectionListView.PinnedSectionListAdapter;

import android.R.id;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v4.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @name WillPlayAdapter
 * @Descripation 即将上映的影片的信息适配器<br>
 * @author 禹慧军
 * @date 2014-11-3
 * @version 1.0
 */
public class WillPlayMovieAdapter extends BaseAdapter implements
		PinnedSectionListAdapter {
	private ArrayList<PlayingMovieList> data;
	private ArrayList<PlayingMovieList> facorite = new ArrayList<PlayingMovieList>();
	private ArrayList<PlayingMovieList> thisWeek = new ArrayList<PlayingMovieList>();
	private ArrayList<PlayingMovieList> nextWeek = new ArrayList<PlayingMovieList>();
	private Context context;
	private LruCache<String, Bitmap> lruCache;
	private ArrayList<Item> items = new ArrayList<Item>();
	String[] contury = new String[] { "最受关注", "本周上映", "下周上映" };
	private static final int CATEGORY_NUM = 3;;

	public WillPlayMovieAdapter(Context context) {
		this.context = context;
		lruCache = ImageCache.GetLruCache(context);
		data = new ArrayList<PlayingMovieList>();
		// generateDataset();
	}

	public void upfavorite(ArrayList<PlayingMovieList> favorite) {
		this.facorite = favorite;
		System.out.println("--------0--favorite" + facorite.size());
		data.addAll(0, favorite);
		synchronized (items) {
			Item section = new Item(Item.SECTION, contury[0]);
			items.add(section);
			for (int j = 0; j < facorite.size(); j++) {
				Item item = new Item(Item.ITEM, "");
				items.add(item);
			}
		}

	}

	public void upnextWeek(ArrayList<PlayingMovieList> nextWeek) {
		this.nextWeek = nextWeek;
		synchronized (items) {
			Item section = new Item(Item.SECTION, contury[2]);
			items.add(section);
			for (int j = 0; j < nextWeek.size(); j++) {
				Item item = new Item(Item.ITEM, "");
				items.add(item);
			}
		}
		data.addAll(thisWeek.size(), nextWeek);
	}

	public void upthisWeek(ArrayList<PlayingMovieList> thisWeek) {
		this.thisWeek = thisWeek;
		System.out.println("--------1--thisWeek" + thisWeek.size());
		synchronized (items) {
			Item section = new Item(Item.SECTION, contury[1]);
			items.add(section);
			for (int j = 0; j < thisWeek.size(); j++) {
				Item item = new Item(Item.ITEM, "");
				items.add(item);
			}
		}

		data.addAll(facorite.size(), thisWeek);
	}

	@Override
	public int getCount() {
		
		return items.size()-CATEGORY_NUM;

	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (items.get(position).type == Item.SECTION) {
			TextView view = view = (TextView) LayoutInflater.from(context)
					.inflate(R.layout.category_title, null);
			view.setTag("" + position);
			/*
			 * if (position==0) { view.setText("最受关注"); }else if (position ==
			 * facorite.size()+1) { view.setText("本周上映"); }
			 */
			view.setText(items.get(position).text);
			return view;
		} else {
			ViewHoder viewHoder = null;
			if (convertView == null) {
				viewHoder = new ViewHoder();
				convertView = LayoutInflater.from(context).inflate(
						R.layout.will_play_item, null);
				viewHoder.conten = (TextView) convertView
						.findViewById(R.id.movie_information);
				viewHoder.img = (ImageView) convertView
						.findViewById(R.id.movie_image);
				viewHoder.isnew = (ImageView) convertView
						.findViewById(R.id.movie_tag2);
				viewHoder.level = (TextView) convertView
						.findViewById(R.id.movie_wish);
				viewHoder.name = (TextView) convertView
						.findViewById(R.id.movie_title);
				viewHoder.summary = (TextView) convertView
						.findViewById(R.id.movie_summary);
				viewHoder.tag = (ImageView) convertView
						.findViewById(R.id.movie_tag1);
				convertView.setTag(viewHoder);
			} else {
				viewHoder = (ViewHoder) convertView.getTag();
			}
			PlayingMovieList plaingMovieList = data.get(position);
			viewHoder.conten.setText(plaingMovieList.getContent());
			viewHoder.level.setText(plaingMovieList.getWish() + "人想看");
			viewHoder.name.setText(plaingMovieList.getName());
			viewHoder.summary.setText(plaingMovieList.getSummmary());
			String tag1 = plaingMovieList.getTag();
			if (tag1.contains("IMAX 3D")) {
				viewHoder.tag.setImageResource(R.drawable.ic_3d_imax);
			} else if (tag1.contains("2D")) {
				viewHoder.tag.setImageResource(R.drawable.ic_2d_imax);
			} else if (tag1.contains("3D")) {
				viewHoder.tag.setImageResource(R.drawable.ic_3d);
			}
			viewHoder.img.setTag(plaingMovieList.getImg());
			viewHoder.img.setImageResource(R.drawable.bg_default_cat_gray);
			new ImageCache(context, lruCache, viewHoder.img,
					plaingMovieList.getImg(), "CatEyeMovie", 90, 120);
			return convertView;
		}

		/*
		 * TextView view = (TextView) super.getView(position, convertView,
		 * parent);
		 */
		/*
		 * Item item = getItem(position); if (item.type == Item.SECTION) { //
		 * view.setOnClickListener(PinnedSectionListActivity.this);
		 * view.setBackgroundColor(0xffcccccc); }
		 */

	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public int getItemViewType(int position) {
		return items.get(position).type;
	}

	@Override
	public boolean isItemViewTypePinned(int viewType) {

		return viewType == Item.SECTION;

	}

	static class Item {

		public static final int ITEM = 0;
		public static final int SECTION = 1;
		public final int type;
		public final String text;

		public int sectionPosition;
		public int listPosition;

		public Item(int type, String text) {
			this.type = type;
			this.text = text;
		}

		@Override
		public String toString() {
			return text;
		}

	}

	private class ViewHoder {
		TextView name;
		TextView summary;
		TextView conten;
		TextView level;
		ImageView img;
		ImageView tag;
		ImageView isnew;
		Button buy;
	}

}
