package com.yuhj.cateyemovie.bean;

/**
 * @name User
 * @Descripation 人 的实体信息<br>
 * @author 禹慧军
 * @date 2014-11-4
 * @version 1.0
 */
public class User {

	public User() {
		// TODO Auto-generated constructor stub
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 姓名
	 */
	private String name;
	
	/**
	 * 用户ID
	 */
	private String id;
	
	
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	private String image;

}
