package com.yuhj.cateyemovie.bean;

/**
 * @name Image
 * @Descripation 图片实体<br>
 * @author 禹慧军
 * @date 2014-11-4
 * @version 1.0
 */
public class Image {

	public Image() {
		// TODO Auto-generated constructor stub
	}
	
	private String ImageUrl;

	public String getImageUrl() {
		return ImageUrl;
	}

	public void setImageUrl(String imageUrl) {
		ImageUrl = imageUrl;
	}

}
