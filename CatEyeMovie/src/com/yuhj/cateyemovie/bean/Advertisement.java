package com.yuhj.cateyemovie.bean;

import android.R.integer;

/**
 * @name Advertisement
 * @Descripation 广告<br>
 * @author 禹慧军
 * @date 2014-11-1
 * @version 1.0
 */
public class Advertisement {

	public Advertisement() {
		// TODO Auto-generated constructor stub
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	private long id;
	
	private String name;
	
	private String img;

}
