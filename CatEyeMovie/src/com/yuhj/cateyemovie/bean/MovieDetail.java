package com.yuhj.cateyemovie.bean;

/**
 * @name MovieDetail
 * @Descripation 电影实体的信息<br>
 * @author 禹慧军
 * @date 2014-11-4
 * @version 1.0
 */
public class MovieDetail {

	public MovieDetail() {
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public float getLevel() {
		return level;
	}

	public void setLevel(float level) {
		this.level = level;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public String getSummarry() {
		return summarry;
	}

	public void setSummarry(String summarry) {
		this.summarry = summarry;
	}

	public String getsNum() {
		return sNum;
	}

	public void setsNum(String sNum) {
		this.sNum = sNum;
	}

	public String getHowlongtime() {
		return howlongtime;
	}

	public void setHowlongtime(String howlongtime) {
		this.howlongtime = howlongtime;
	}

	public String getStar() {
		return star;
	}

	public void setStar(String star) {
		this.star = star;
	}

	/**
	 * 姓名
	 */
	private String name;
	
	/**
	 * ID
	 */
	private long id;
	
	/**
	 * 上映时间
	 */
	private String time;
	
	/**
	 * 类型
	 */
	private  String cat;
	
	
	/**
	 * 评分
	 */
	private float level;
	
	
	/**
	 * 导演
	 */
	private String director;
	
	
	/**
	 * 国家
	 */
	private String country;
	
	
	/**
	 * 类型3d/2d
	 */
	private String ver;
	
	/**
	 * 摘要
	 */
	private String summarry;
	
	
	/**
	 * 评分人数
	 */
	private String sNum;
	
	
	/**
	 * 片长
	 */
	private String howlongtime;
	
	/**
	 * 演员
	 */
	private String star;
	
	/**
	 * 图片个数
	 */
	private int picCount;
	
	public int getPicCount() {
		return picCount;
	}

	public void setPicCount(int picCount) {
		this.picCount = picCount;
	}

	private String[] images;

	public String[] getImages() {
		return images;
	}

	public void setImages(String[] images) {
		this.images = images;
	}
	
	private String img;

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}
	

}
