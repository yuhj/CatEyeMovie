package com.yuhj.cateyemovie.bean;

/**
 * @name Comments
 * @Descripation <br>
      评论实体的信息
 * @author 禹慧军
 * @date 2014-11-4
 * @version 1.0
 */
public class Comments {

	public Comments() {
		// TODO Auto-generated constructor stub
	}
    	private long id;
	
	    public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

		public String getComment() {
			return comment;
		}

		public void setComment(String comment) {
			this.comment = comment;
		}

		public String getTime() {
			return time;
		}

		public void setTime(String time) {
			this.time = time;
		}

		public long getApprove() {
			return approve;
		}

		public void setApprove(long approve) {
			this.approve = approve;
		}

		public long getReply() {
			return reply;
		}

		public void setReply(long reply) {
			this.reply = reply;
		}
		private User user;
	    
	    private String comment;
	    
	    private String time;
	    
	    private long  approve;
	    
	    private long  reply;
	    
	    public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}
		private String title;
}
