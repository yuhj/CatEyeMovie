package com.yuhj.cateyemovie.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.baidu.mapapi.SDKInitializer;
import com.yuhj.cateyemovie.R;
import com.yuhj.cateyemovie.fragment.CinemaFragment;
import com.yuhj.cateyemovie.fragment.HomeFragment;
import com.yuhj.cateyemovie.fragment.MovieFragment;
import com.yuhj.cateyemovie.fragment.MyFragment;
import java.util.ArrayList;

public class MainActivity extends FragmentActivity implements
		RadioGroup.OnCheckedChangeListener {
	private CinemaFragment cinemaFragment;
	private ArrayList<Fragment> fragments;
	private HomeFragment homeFragment;
	private MovieFragment movieFragment;
	private MyFragment myFragment;
	private RadioGroup radioGroup;
	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		SDKInitializer.initialize(getApplicationContext());
		setContentView(R.layout.activity_main);
		initView();
		FragmentTransaction localFragmentTransaction = getSupportFragmentManager()
				.beginTransaction();
		Bundle bundle =new Bundle();
		Intent intent =getIntent();
		String city = intent.getStringExtra("city");
		bundle.putString("city",city);
		fragments.get(0).setArguments(bundle);
		localFragmentTransaction.replace(R.id.main_framelayout,
				(Fragment) this.fragments.get(0));
		localFragmentTransaction.commit();
	
	}
	private void initView() {
		this.movieFragment = new MovieFragment();
		this.myFragment = new MyFragment();
		this.homeFragment = new HomeFragment();
		this.cinemaFragment = new CinemaFragment();
		this.radioGroup = ((RadioGroup) findViewById(R.id.main_tab_bar));
		this.radioGroup.setOnCheckedChangeListener(this);
		this.fragments = new ArrayList();
		this.fragments.add(this.movieFragment);
		this.fragments.add(this.cinemaFragment);
		this.fragments.add(this.homeFragment);
		this.fragments.add(this.myFragment);
	}

	public void onCheckedChanged(RadioGroup RadioGroup, int paramInt) {
		int childCount = radioGroup.getChildCount();
		int checkedIndex = 0;
		RadioButton btnButton = null;
		for (int i = 0; i < childCount; i++) {
			btnButton = (RadioButton)RadioGroup.getChildAt(i);
			if (btnButton.isChecked()) {
				checkedIndex = i;
				break;
			}
		}
		Fragment fragment =fragments.get(checkedIndex);
		FragmentManager manager =getSupportFragmentManager();
		FragmentTransaction transaction =manager.beginTransaction();
		
		transaction.replace(R.id.main_framelayout,fragment);
		
		transaction.commit();
	}
	
	private long exitTime = 0;

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN){   
	        if((System.currentTimeMillis()-exitTime) > 2000){  
	            Toast.makeText(getApplicationContext(), "再按一次退出程序", Toast.LENGTH_SHORT).show();                                
	            exitTime = System.currentTimeMillis();   
	        }else {
	            finish();
	            System.exit(0);
	        }
	        return true;   
	    }
	    return super.onKeyDown(keyCode, event);
	}


}