package com.yuhj.cateyemovie.activitys;

import java.util.ArrayList;

import com.yuhj.cateyemovie.R;
import com.yuhj.cateyemovie.R.id;
import com.yuhj.cateyemovie.R.layout;
import com.yuhj.cateyemovie.adapter.ImageGridAdapter;
import com.yuhj.cateyemovie.clients.ClientApi;
import com.yuhj.cateyemovie.interfaces.ImageCallback;
import com.yuhj.cateyemovie.utils.LoadingAinm;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.R.integer;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ImageGridActivity extends Activity {
	private String movie_id ;
	private ImageGridAdapter adapter;
	private TextView title;
	private RelativeLayout loadRelativeLayout;
	private String url ;
	private Handler handler = new Handler() {

		public void handleMessage(android.os.Message msg) {
			if (msg == null) {
				loadRelativeLayout.setVisibility(View.GONE);
				Toast.makeText(getApplicationContext(), "网络异常，请检查！", 1).show();
			} else {
				loadRelativeLayout.setVisibility(View.GONE);
				gridView.setVisibility(View.VISIBLE);
				adapter = new ImageGridAdapter(getApplicationContext(),
						(ArrayList<String>) msg.obj);
				gridView.setAdapter(adapter);
				adapter.notifyDataSetChanged();
			}
		}

	};
	private GridView gridView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_image_grid);
		movie_id=getIntent().getStringExtra("id");
		url = "http://api.mobile.meituan.com/dianying/movie/"
		+ movie_id
		+ "/photos.json?utm_campaign=AmovieBmovieC110189035512576D-1&movieBundleVersion=100&utm_source=wandoujia&utm_medium=android&utm_term=100&utm_content=352706060172190&ci=1&uuid=A1A230538BAA987DFF805E95D3B8D53EAB6025EC580F2C6C56CF303DD5BB5A2E";
		gridView = (GridView) findViewById(R.id.image_gridView);
		String titleString = getIntent().getStringExtra("title");
		title  = (TextView) findViewById(R.id.movie_name);
		title.setText(titleString);
		
		loadRelativeLayout=(RelativeLayout) findViewById(R.id.lodingRelativeLayout);
		LoadingAinm.ininLoding(ImageGridActivity.this);
		new Thread(new Runnable() {

			@Override
			public void run() {
				ClientApi.getSmallImage(url, new ImageCallback() {

					@Override
					public void getresult(ArrayList<String> result) {
						Message msg = Message.obtain();
						msg.obj = result;
						handler.sendMessage(msg);
					}
				});
			}
		}).start();
		
		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent =new Intent(ImageGridActivity.this,ImageViewPagerActivity.class);
				intent.putExtra("position", position);
				intent.putExtra("id",movie_id+"");
				startActivity(intent);
			}
		});

	}

}
