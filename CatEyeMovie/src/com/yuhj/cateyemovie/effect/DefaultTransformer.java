package com.yuhj.cateyemovie.effect;

import android.support.v4.view.ViewPager.PageTransformer;
import android.view.View;

/**
 * @name DefaultTransformer
 * @Descripation <br>
 *               默认动画
 * @author 禹慧军
 * @date 2014-11-5
 * @version 1.0
 */
public class DefaultTransformer implements PageTransformer {

	@Override
	public void transformPage(View view, float arg1) {
		view.setAlpha(1);
		view.setTranslationX(0);
		view.setTranslationY(0);
		view.setPivotX(view.getWidth() / 2);
		view.setPivotY(view.getHeight() / 2);
		view.setScaleX(1);
		view.setScaleY(1);
		view.setRotation(0);
	}

}
