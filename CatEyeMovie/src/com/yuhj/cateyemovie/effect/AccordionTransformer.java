package com.yuhj.cateyemovie.effect;

import android.support.v4.view.ViewPager.PageTransformer;
import android.view.View;

import com.nineoldandroids.view.ViewHelper;

/**
 * @name AccordionTransformer
 * @Descripation <br>
 *               左右折叠
 * @author 禹慧军
 * @date 2014-11-5
 * @version 1.0
 */
public class AccordionTransformer implements PageTransformer {

	@Override
	public void transformPage(View view, float position) {
		if (position < -1) {
			ViewHelper.setPivotX(view, view.getMeasuredWidth() * 0.5f);
			ViewHelper.setPivotY(view, view.getMeasuredHeight() * 0.5f);
			ViewHelper.setScaleX(view, 1);
		} else if (position <= 0) {
			ViewHelper.setPivotX(view, view.getMeasuredWidth());
			ViewHelper.setPivotY(view, 0);
			ViewHelper.setScaleX(view, 1 + position);
		} else if (position <= 1) {
			ViewHelper.setPivotX(view, 0);
			ViewHelper.setPivotY(view, 0);
			ViewHelper.setScaleX(view, 1 - position);
		} else {
			ViewHelper.setPivotX(view, view.getMeasuredWidth() * 0.5f);
			ViewHelper.setPivotY(view, view.getMeasuredHeight() * 0.5f);
			ViewHelper.setScaleX(view, 1);
		}
	}
}
